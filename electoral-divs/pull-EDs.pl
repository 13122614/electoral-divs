#!/usr/bin/perl -w

# @INC = (Geo-ShapeFile-2.52/lib", @INC);

use Geo::ShapeFile;
use Geo::ShapeFile::Shape;
use Data::Dumper;

# my $shapefile = new Geo::ShapeFile("electoral_divisions");
my $shapefile = new Geo::ShapeFile("Census2011_Electoral_Divisions_generalised20m.shp");

for(1 .. $shapefile->shapes()) {
  my $shape = $shapefile->get_shp_record($_);
  # see Geo::ShapeFile::Shape docs for what to do with $shape

#  print Dumper [$shape];
  print $shape->dump();
#  my %db = $shapefile->get_dbf_record($_);
}
