#!/usr/bin/perl -w
use strict;

## Usage: perl basic_constituency_builder_max.pl > basicConstituencyMax

sub buildConstituency($);

use Geo::ShapeFile::Point comp_includes_z => 0, comp_includes_m => 0;
use Geo::ShapeFile;
use List::Util qw(shuffle);


my $shpFile = shift() || "Census2011_Electoral_Divisions_generalised20m.shp";
my $shpObj = new Geo::ShapeFile($shpFile);

my %ed2pop;
my @weightArray;
my @usedED;
my $sumPop;
my @neighbours;
my $run = 1;
## maxPop and acceptableRange can be altered to test lower bound for each constituency
## Example: Constituency of 60,000 would have 3 TDs
my $maxPop = 150000;
my $acceptableRange = 0;
my $neighbourCount = 0;
my $constituencyCount = 1;
my $totalEDs = 3409;
my $edgeValue = 2;
my $tdPerCon = 3;
my @EDOrder = shuffle 1..$totalEDs;

open POPLIST, 'pop_list' or die $!;
while(<POPLIST>)
{
  chomp $_;
  my @pop = split(/,/, $_);
  $ed2pop{$pop[0]} = $pop[1];
  push(@usedED, 0);
}
close POPLIST or die $!;

open WEIGHTLIST, 'weighted_list' or die $!;
while(<WEIGHTLIST>)
{
  chomp $_;
  push(@weightArray, $_);
}
close WEIGHTLIST;

my $i = 0;
push(@neighbours, $EDOrder[$i]);
$sumPop = $ed2pop{$EDOrder[$i]};
$usedED[$EDOrder[$i] - 1] = 1;

while($i < $totalEDs)
{
  while($neighbourCount <= $#neighbours)
  {
    if($sumPop < ($maxPop + $acceptableRange))
    {
      buildConstituency($neighbours[$neighbourCount]);
      $neighbourCount++;
    }
    if($neighbourCount > $#neighbours && $edgeValue > 0)
    {
      $neighbourCount = 0;
      $edgeValue--;
    }
  }
  print "Constituency no. $constituencyCount\n"; 
  print "total no. of EDs in constituency: ", $#neighbours + 1, "\n";;
  print "total population: $sumPop\n\n";
  foreach (@neighbours)
  {
    print "$_.\t${$shpObj->get_dbf_record($_)}{'EDNAME'}\n"
  }
  print "\n##########################\n\n";
  $constituencyCount++;
  $edgeValue = 2;
  $neighbourCount = 0;
  @neighbours = ();
  while(($i < $totalEDs) && ($usedED[$EDOrder[$i] - 1] == 1))
  {
    $i++;
  }
  if(($i < $totalEDs) && ($usedED[$EDOrder[$i] - 1] == 0))
  {
    push(@neighbours, $EDOrder[$i]);
    $sumPop = $ed2pop{$EDOrder[$i]};
    $usedED[$EDOrder[$i] - 1] = 1;
  }
}

print "Total no. of Constituencies: ", --$constituencyCount, "\n";


sub buildConstituency($)
{
  my $edInFocus = shift() or die $!;
  foreach (@weightArray)
  {
    if($_ =~ /^$edInFocus,$edgeValue,/)
    {
      my @split = split(/,/, $_);
      if($usedED[$split[2] - 1] == 0 && ($sumPop + $ed2pop{$split[2]}) <= ($maxPop + $acceptableRange))
      {
	$sumPop += $ed2pop{$split[2]};
	$usedED[$split[2] - 1] = 1;
	push(@neighbours, $split[2]);
      }
    }
  }
}