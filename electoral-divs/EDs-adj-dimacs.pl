#!/usr/bin/perl -w

use strict;

my $nodect = -1;
my $edgect;
while (<STDIN>)
{
    next if /^\s*#/;

    chomp;
    if ($nodect == -1 &&
	/^\s*(\d+)\s*(\d+)\s*$/)
    {
	$nodect = $1;
	$edgect = $2;
	print "p edge $nodect $edgect\n";
	next;
    }

    my ($u, @adjs) = split /\s+/;
    foreach my $v (@adjs)
    {
	print "e $u $v\n";
    }
}
