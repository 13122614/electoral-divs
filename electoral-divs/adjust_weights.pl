#!/usr/bin/perl -w
use strict;

## Usage: perl adjust_weights.pl 'ED 1' 'new edge value' 'ED 2'

sub findED($);

use Geo::ShapeFile::Point comp_includes_z => 0, comp_includes_m => 0;
use Geo::ShapeFile;

my $shpFile = "Census2011_Electoral_Divisions_generalised20m.shp";
my $shpObj = new Geo::ShapeFile($shpFile);

my $filename = "weighted_list";
my @ids1, my @ids2;
my $id1, my $id2;
my $n, my $name1, my $name2;
my $adjacent = 0;
if($#ARGV != 2)
{
  print "USAGE: perl adjust_weights.pl 'ED name 1' 'new edge value' 'ED name 2'\n";
  exit;
}
if($ARGV[1] =~ /[0-9]/)
{
  @ids1 = findED($ARGV[0]);
  if($#ids1 >= 0)
  {
    $name1 = ${$shpObj->get_dbf_record($ids1[0])}{'EDNAME'};
    @ids2 = findED($ARGV[2]);
    if($#ids2 == -1)
    {
      print "Incorrect input: 'ED name 2'\n";
      exit;
    }
    else
    {
      $name2 = ${$shpObj->get_dbf_record($ids2[0])}{'EDNAME'};
    }
  }
  else
  {
    print "Incorrect input: 'ED name 1'\n";
    exit;
  }
}
else
{
  print "Second argument must be an integer (0-9)\n";
  exit;
}

open WEIGHTLIST, $filename or die $!;
my @weightArray;
while(<WEIGHTLIST>)
{
  chomp $_;
  push(@weightArray, $_);
}
close WEIGHTLIST;

my $i = 0, my $j = 0;
while($i <= $#ids1 && !$adjacent)
{
  while($j <= $#ids2 && !$adjacent)
  {
    foreach (@weightArray)
    {
      if($_ =~ /$ids1[$i],[0-9],$ids2[$j]/)
      {
	my @split1 = split(/,/, $_);
	if(($ids1[$i] == $split1[0]) && ($ids2[$j] == $split1[2]))
	{
	  $_ = "$ids1[$i],$ARGV[1],$ids2[$j]";
	  $adjacent = 1;
	}
      }
      if($_ =~ /$ids2[$j],[0-9],$ids1[$i]/)
      {
	my @split2 = split(/,/, $_);
	if(($ids2[$j] == $split2[0]) && ($ids1[$i] == $split2[2]))
	{
	  $_ = "$ids2[$j],$ARGV[1],$ids1[$i]";
	  $adjacent = 1;
	}
      }
    }
    if($adjacent)
    {
      $id1 = $ids1[$i];
      $id2 = $ids2[$j];
      last;
    }
    else
    {
      $j++;
    }
  }
  if(!$adjacent)
  {
    $i++;
  }
}

if(!$adjacent)
{
  print "$name1 and $name2 are not adjacent.\n";
  exit;
}
else
{
  ##write back to file ##
  open OUTPUT, "> $filename" or die $!;
  foreach (@weightArray)
  {
    print OUTPUT "$_\n";
  }
  close OUTPUT;

  print "File: \'$filename\' has been updated.\n";
}
sub findED($)
{
  $n = shift or die $!;
  my @ids = ();
  my $totalEDS = 3409;
  my $id = 1;
  while($id <= $totalEDS)
  {
    if((c $n) eq (lc ${$shpObj->get_dbf_record($id)}{'EDNAME'}))
    {
      push(@ids, $id);
    }
    $id++;
  }
  
  @ids;
}