const command = "echo"
# argument format: (<argument>, [<list>, <of>, <parameters>]) For now, emptystrings are a stand-in for nothing, and are removed before execution
args = [
("-n", ["1", "2", "3", ""]),
("-e", ["a", "b", "c"]),
("-e", ["a", "b", "c"]),
]
remove_blanks = true # set this to false for a speedup if you have no optional parameters for arguments
