#=
	Ths program generates an exaustive list of arguments
	for a givven program and, optionally, runs the program
	with these arguments.

	
	NB: this needs bothe the Iterators and Combinatorics packages to work. Rather than adding code to install them if they're not present, I'll just put this warning here.
	run the following two lines to download and install the packages;
	Pkg.add("Iterators")
	Pkg.add("Combinatorics")

	p.s. combinatorics is not fully implemented in 0.4.X, uncomment the using statement when running under 0.5 or higher

=#



import Base: combinations
#using Combinatorics
combinations(a) = chain([combinations(a,k) for k=1:length(a)]...)
using Iterators


include("config.jl")


println("Starting generaton: ", now())


#hardcode the case with no arguments
cmds = [`$command`]

for comb in combinations(args)
	for prod in product(map(x -> [(x[1], y) for y in x[2]], comb)...)
		push!(cmds, `$command $(vcat(map(collect, prod)...))`)
	end
end


if remove_blanks
	for i in cmds
		filter!(x -> x != "" , i.exec)
	end
end


#execute
println("Starting running: ", now())
for i in cmds
	run(i)
end
println("Finished: ", now())