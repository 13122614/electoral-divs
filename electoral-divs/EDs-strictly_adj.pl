#!/usr/bin/perl -w

## amended EDs-adjacancies.pl
## testing with less accuracy for common vertices between polygons
## just print ID numbers
## For use with EDs-weightedEdges.pl

use strict;
use Geo::ShapeFile::Point comp_includes_z => 0, comp_includes_m => 0;
use Geo::ShapeFile;
use Math::Round;
use Data::Dumper;

my $sfile = shift() || "Census2011_Electoral_Divisions_generalised20m.shp";
my $obj = new Geo::ShapeFile($sfile);


my (%pt2ed) = ();
while (<STDIN>)
{
  ## Going to try and shorten point coordinates to three decimal places.
  ## This may give us the missing adjacancy listings in the adjacancies.pl file.
  ## NOTE: As intended, 25 of the missing EDs have now been correctly accounted for.
  ## NOTE: The remaining 7 EDs are isolated islands with no adjacent EDs.
    my ($key, $shpId) = split /\s+/;
    
    my @intermid = split (/,/, $key);
    #truncating points to 3 decimal places
    $intermid[0] = nearest(.001, $intermid[0]);
    $intermid[1] = nearest(.001, $intermid[1]);
    $key = "$intermid[0],$intermid[1]";
    push @{$pt2ed{$key}}, $shpId;
}
# now do an inefficient cull of the same EDs appearing more than once
# snagged from http://perlmaven.com/unique-values-in-an-array-in-perl
foreach my $key (keys %pt2ed)
{
    @{$pt2ed{$key}} = do { my %seen; grep { !$seen{$_}++ } @{$pt2ed{$key}} };
    @{$pt2ed{$key}} < 2 && next;
}

my $edgeCt = 0;
my (%adjs) = ();
foreach my $key (sort keys %pt2ed)
{
    foreach my $ed1 (@{$pt2ed{$key}})
    {
	foreach my $ed2 (@{$pt2ed{$key}})
	{
	    $ed1==$ed2 && next;
	    $adjs{$ed1}{$ed2} = $adjs{$ed2}{$ed1} = 1;
	    $edgeCt++;
	}
    }
}

#
# now print out adjs in DIMACS format
my $nodeCt = scalar keys %adjs;

my $top;
my $topCt = 0;
foreach my $u (sort {$a <=> $b} keys %adjs)
{
    my $adjCt = scalar keys %{$adjs{$u}};
    print "$u\t", join(' ', sort {$a <=> $b} keys %{$adjs{$u}}), "\n";
    $edgeCt += $adjCt;

    $topCt < $adjCt || next;

    $topCt = $adjCt;
    $top = $u;
}


