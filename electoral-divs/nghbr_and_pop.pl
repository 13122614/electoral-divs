#!/usr/bin/perl -w

## Prints out name of Electoral Division, name of all adjacent Electoral Divisions
## along with each ED's respective total population and the overall total population
## of the listed EDs.
## Code borrowed from EDs-adj_less_acc.pl
## Usage: perl EDs-points.pl | perl nghbr_and_pop.pl > "file name"

use strict;
use Geo::ShapeFile::Point comp_includes_z=> 0, comp_includes_m => 0;
use Geo::ShapeFile;
use Data::Dumper;
use Math::Round;

my $sfile = shift() || "Census2011_Electoral_Divisions_generalised20m.shp";
my $obj = new Geo::ShapeFile($sfile);

print "#\n# List of all Electoral Divisions with (any|all) respective adjacent neighbours.\n";
print "# For each ED, the respective population is given, and the total population\n";
print "# of each cluster is also given.\n\n";
print "##############################################\n\n";

my (%pt2ed) = ();
# truncate point coordinate values.
# fixes missing adjacencies problem.
while (<STDIN>){
  my ($key, $shpId) = split /\s+/;
  my @intermid = split(/,/, $key);
  $intermid[0] = nearest(.001, $intermid[0]);
  $intermid[1] = nearest(.001, $intermid[1]);
  
  $key = "$intermid[0],$intermid[1]";
  push @{$pt2ed{$key}}, $shpId;
}
# inefficient cull of the same EDs
foreach my $key (keys %pt2ed){
  @{$pt2ed{$key}} = do {my %seen; grep {!$seen{$_}++} @{$pt2ed{$key}} };
  @{$pt2ed{$key}} < 2 && next;
}
my $edgeCt = 0;
my (%adjs) = ();
foreach my $key (sort keys %pt2ed){
  foreach my $ed1 (@{$pt2ed{$key}}){
    foreach my $ed2 (@{$pt2ed{$key}}){
      $ed1 == $ed2 && next;
      $adjs{$ed1}{$ed2} = $adjs{$ed2}{$ed1} = 1;
      $edgeCt++;
    }
  }
}

my $top;
my $topCt = 0;
my $sid = 1;
foreach my $u (sort {$a <=> $b} keys %adjs){
  ## population record code specific to 2011 Census shp file
  my $pop = int (${$obj->get_dbf_record($sid)}{'Total2011'});
  print "##############################################\n\n";
  print "No. $sid\tName: ${$obj->get_dbf_record($sid)}{'EDNAME'}\tPopulation: $pop\n";
  
  if($u != $sid){
    print "\nNeighbours: None\n";
    print "\nTotal Population: $pop\n\n";
    $sid++;
    redo;
  }
  else{
    my @neighbours = sort {$a <=> $b} keys %{$adjs{$u}};
    print "\nNeighbours:\n";
    for my $i(@neighbours){
    ## population record code specific to 2011 Census shp file
      my $nghbrpop = int (${$obj->get_dbf_record($i)}{'Total2011'});
      print "No. $i\tName: ${$obj->get_dbf_record($i)}{'EDNAME'}\tPopulation: $nghbrpop\n";
      $pop += $nghbrpop;
    }
    print "\nTotal Population: $pop\n\n";
    $sid++;
  }
  
}











