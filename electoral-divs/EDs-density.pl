#!/usr/bin/perl -w
use strict;

## Usage: perl EDs-Pop-Area.pl | perl EDs-density.pl > densities

use Geo::ShapeFile::Point comp_includes_z => 0, comp_includes_m => 0;
use Geo::ShapeFile;

my %id2den = ();
my (@split, @names);
my $d = 0;
print "## List of EDs from densest to sparsest ##\n\n";
while(<STDIN>){
  @split = split(', ', $_);
  push @names, $split[1];
  $d = $split[2] / $split[3];
  $id2den{$split[0]} = $d;
}

## hash value sort code taken from: alvinalexander.com/perl/edu/qanda/plqa00016
foreach my $key (sort descOrder (keys(%id2den))){
  print "$key, $names[$key - 1], $id2den{$key}\n";
}

sub descOrder{
  $id2den{$b} <=> $id2den{$a};
}