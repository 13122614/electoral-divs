#!/usr/bin/perl -w
use strict;

## Usage: perl ED_pop_list.pl > pop_list

use Geo::ShapeFile::Point comp_includes_z => 0, comp_includes_m => 0;
use Geo::ShapeFile;

my $sfile = shift() || "Census2011_Electoral_Divisions_generalised20m.shp";
my $obj = new Geo::ShapeFile($sfile);

my $totalEDs = $obj-> shapes();

# -Print out ID, Name, Population, Area of each ED
# NOTE: Not sure if ID shoud coincide with numbering used in 
#	previous perl programs, such as EDs-adjacancies.
#	-For now, ID used is same as previous perl programs.
#	-Commented out code below prints out CSOED no. as found in .shp file
# NOTE: ED Population derived from 2011 Census; Total2011
# NOTE: ED Area value = Total Area of ED; TOTAL_AREA
my $id = 1;
while($id <= $totalEDs){
  print "$id, ";
  print int(${$obj->get_dbf_record($id)}{'Total2011'}), "\n";
  $id++;
}