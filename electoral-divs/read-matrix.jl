
# Many links are relative to here:
#   https://en.wikibooks.org/wiki/Introducing_Julia

const EDs = 3409;
const Cct = 41;
# A solution will be a char matrix: S[e,c]=1 <==> e \in constit(c)
# However, solution vectors (used in L-BFGS alg) will be linearised / reshaped
#  versions of S
const Vsz = EDs*Cct;            # size of a sol vector

adj = zeros(Int, EDs, EDs)
file = open("weighted-adjs");   # .../Working_with_text_files#Writing_and_reading_array_to_and_from_a_file

for line in eachline(file)  # eachline(STDIN)
    if ismatch(r"^\s*#", line)
        continue
    end
    # .../Strings_and_characters#Splitting_and_joining_strings
    data = map(x->(v=tryparse(Int,x);
                   isnull(v) ? 0 : get(v)),
               split(line, ","));
    # ed = shift!(data);
    # for nbr=data
    #     # println("($ed,$nbr) was $(adj[ed,nbr])");
    #     adj[ed,nbr] = 1;
    # end
    adj[data[1],data[3]] = data[2];
end

if issym(adj)
    println("adj is symmetric ok.")
end

evals = eigvals(adj);
# print(evals);
nonzeros = filter(x -> 1e-10<abs(x), evals);
println("No. of evals: ", length(evals), "; non-zeros:", length(nonzeros));
mn, mx = minimum(evals), maximum(evals)
println("Smallest e'val: ", mn);
println("Largest e'val: ", mx);

for i in 1:EDs
    adj[i,i] = -ceil(Int, mx);
end

if isposdef(-adj)
    println("adj mtx is now negdef.");
end


###############
# http://docs.julialang.org/en/release-0.4/manual/types/

type sol
    partition::Array{Float64,1} # vectorised S
    lambda::Array{Float64,1}    # approximates Lagrange multipliers
    mu::Float64                 # penalty term on
end

s0=sol(rand(Vsz), zeros(Vsz), 1.2)

function partCost(
