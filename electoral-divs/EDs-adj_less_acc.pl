#!/usr/bin/perl -w

## amended EDs-adjacancies.pl
## testing with less accuracy for common vertices between polygons
##

# <12:29:09; Wed,  1 Jun 2016>
#   pH: Added "\n" at three points to break output into separate sections

use strict;
use Geo::ShapeFile::Point comp_includes_z => 0, comp_includes_m => 0;
use Geo::ShapeFile;
use Math::Round;
use Data::Dumper;

my $sfile = shift() || "Census2011_Electoral_Divisions_generalised20m.shp";
my $obj = new Geo::ShapeFile($sfile);

print "#\n# adjacancy graph in adjacancy list format\n";
print "# Three sections:\n";
print "#  1) adjacancy lists;\n";
print "#  2) IDs <--> Names\n";
print "#  3) Points <--> EDs\n";

my (%pt2ed) = ();
while (<STDIN>)
{
  ## Going to try and shorten point coordinates to three decimal places.
  ## This may give us the missing adjacancy listings in the adjacancies.pl file.
  ## NOTE: As intended, 25 of the missing EDs have now been correctly accounted for.
  ## NOTE: The remaining 7 EDs are isolated islands with no adjacent EDs.
    my ($key, $shpId) = split /\s+/;
    
    my @intermid = split (/,/, $key);
    #truncating points to 3 decimal places
    $intermid[0] = nearest(.001, $intermid[0]);
    $intermid[1] = nearest(.001, $intermid[1]);
    $key = "$intermid[0],$intermid[1]";
    push @{$pt2ed{$key}}, $shpId;
}
# now do an inefficient cull of the same EDs appearing more than once
# snagged from http://perlmaven.com/unique-values-in-an-array-in-perl
foreach my $key (keys %pt2ed)
{
    @{$pt2ed{$key}} = do { my %seen; grep { !$seen{$_}++ } @{$pt2ed{$key}} };
    @{$pt2ed{$key}} < 2 && next;
}

my $edgeCt = 0;
my (%adjs) = ();
foreach my $key (sort keys %pt2ed)
{
    foreach my $ed1 (@{$pt2ed{$key}})
    {
	foreach my $ed2 (@{$pt2ed{$key}})
	{
	    $ed1==$ed2 && next;
	    $adjs{$ed1}{$ed2} = $adjs{$ed2}{$ed1} = 1;
	    $edgeCt++;
	}
    }
}

#
# now print out adjs in DIMACS format
my $nodeCt = scalar keys %adjs;
print "\n# 1) adjacancy lists\n"; # extra "\n" to break into sections -- pH
print "$nodeCt $edgeCt\n";

my $top;
my $topCt = 0;
foreach my $u (sort {$a <=> $b} keys %adjs)
{
    my $adjCt = scalar keys %{$adjs{$u}};
    print "$u\t", join(' ', sort {$a <=> $b} keys %{$adjs{$u}}), "\n";
    $edgeCt += $adjCt;

    $topCt < $adjCt || next;

    $topCt = $adjCt;
    $top = $u;
}

print "\n# 2) IDs <--> Names\n"; # extra "\n" to break into sections -- pH
print "# Most surrounded: $top=${$obj->get_dbf_record($top)}{'EDNAME'}\n";
print "#  $top: ", join(' ', sort {$a <=> $b} keys %{$adjs{$top}}), "\n";
foreach my $sid (1..$obj->shapes)
{
    print "# $sid\t${$obj->get_dbf_record($sid)}{'EDNAME'}\n";
    defined $adjs{$sid} ||
	print STDERR " --- no adj list for $sid=${$obj->get_dbf_record($sid)}{'EDNAME'}\n";
}
print "\n# 3) Points <--> EDs\n"; # extra "\n" to break into sections -- pH
foreach my $key (keys %pt2ed)
{
    print "# $key #", scalar @{$pt2ed{$key}}, " ", join(' ', @{$pt2ed{$key}}), "\n";
}
