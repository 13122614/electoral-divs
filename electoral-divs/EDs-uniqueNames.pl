#!/usr/bin/perl -w
#Usage: perl EDs-uniqueNames.pl > uniqueNames
use strict;

use Geo::ShapeFile::Point comp_includes_z => 0, comp_includes_m => 0;
use Geo::ShapeFile;

my $shpFile = shift || "Census2011_Electoral_Divisions_generalised20m.shp";
my $shpObj = new Geo::ShapeFile($shpFile);
my $totalEDs = 3409;
my $id = 1;
my @names, my @uniqueNames;
while($id <= $totalEDs)
{
  push (@names, ${$shpObj->get_dbf_record($id)}{'EDNAME'});
  $id++;
}
@uniqueNames = do {my %seen; grep {!$seen{$_}++} @names};
foreach my $i (@uniqueNames)
{
  print "$i\n";
}