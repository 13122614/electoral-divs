#=
generates output of the form 
ED: pop; CSOED; name; Const; nbr1,v1; nbr2,v2; ...; nbrk,vk

where
 - ED is the ED no. in the range 1--3409
 - pop is the ED's population,
 - CSOED is how it is referred to internally in the Census files,
 - name is as you have reported it,
 - Const is the constituency / partition / grouping that the ED is
   assigned to
 - nbri,vi refers to the ith neighbor of the ED and vi is the strength
   of the association between the two nbrs, where lower strength means
   it is more acceptable to "break" the edge and put the two neighboring
   EDs in /different/ constituencies.


from the files adj_less_acc, weighted_list, and Census2011_Electoral_Divisions_generalised20m.dbf

=#

const adj_list_file = "adj_less_acc"
const weighted_list_file = "weighted_list"
const census_data_file = "Census2011_Electoral_Divisions_generalised20m.dbf"

# matches a record from the dbf file in long form, and captures the relevant information.
const valid_census_record_regex = r"\n?Record: (\d+)\nNUTS1: [\w,\ \-\)\(áéíóú]+\nNUTS1NAME: [\w,\ \-\)\(áéíóú]+\nNUTS2: [\w,\ \-\)\(áéíóú]+\nNUTS2NAME: [\w,\ \-\)\(áéíóú]+\nNUTS3: IE\d{3}\nNUTS3NAME: [\w,\ \-\)\(áéíóú]+\nCOUNTY: \d+\nCOUNTYNAME: ([\w,\ \-\)\(áéíóú]+)\nCSOED: ([\d/]+)\nOSIED: [\d/]+\nEDNAME: (.+)\nMale\d{4}: [\d\.]+\nFemale\d{4}: [\d\.]+\nTotal\d{4}: ([\.\d]+)\nPPOcc\d{4}: [\d\.]+\nUnocc\d{4}: [\d\.]+\nHS\d{4}: [\d\.]+\nVacant\d{4}: [\d\.]+\nPCVac\d{4}: [\d\.]+\nTOTAL_AREA: [\d\.]+\nLAND_AREA: [\d\.]+\nCREATEDATE: \d\d-\d\d-\d{4}"


# this type represents a record stored in the census data file
type ElectoralDivision
	ed_num::Integer # the record number+1, unless otherwise mentioned
	pop::Integer # this is a float in the dbf file, but it's always a whole number
	csoed::AbstractString 
	name::AbstractString # EDNAME
	constituency::AbstractString # I assume this is COUNTYNAME, if this is wrong I'll have to move the capturing group
	neighbors::Array{Tuple{Int, Int}} # found in the weighted_list file, list of neighbors and their strength of associativity
end


# convenience method to construct an ElectoralDivision from a record
function creatediv(str::AbstractString)
	m = match(valid_census_record_regex, str)

	if typeof(m) != RegexMatch
		throw("Error: the value of str is not a valid record.\n\n$str")
	end

	ed_num = parse(Int, m.captures[1]) + 1
	pop = convert(Integer, parse(Float64, m.captures[5]))
	csoed = m.captures[3]
	name = m.captures[4]
	constituency = m.captures[2]

	new_ed = ElectoralDivision(ed_num, pop, csoed, name, constituency, Array{Tuple{Int, Int}}(0))

end



# read the file, convert it to UTF8, squash multiple spaces, remove spaces before a newline, and split the string along blank lines
census_data = split( replace( replace( readall( pipeline(`dbfdump -m $census_data_file`, `iconv -f ISO-8859-15 -t UTF-8`) ), r" {2,}", " "), r" +\n", "\n"), r"\n\n", keep=false)
# read the list of weights as a csv file
weights = readcsv("weighted_list", Int)


electoral_divisions = ElectoralDivision[]


# add each record to the end of the array
for ed in census_data
	push!(electoral_divisions, creatediv(ed))
end


# add the neighbors to the relevant divisions
for i in 1:length(weights[:,1])
	push!(electoral_divisions[weights[i, 1]].neighbors, (weights[i, 3], weights[i, 2]))
end


#print out all of the electoral divisions
for e in electoral_divisions
	
	a = e.ed_num
	b = e.pop
	c = e.csoed
	d = e.name
	f = e.constituency

	print("$a: $b; $c; $d; $f")
	for n in e.neighbors
		nbr = n[1]
		v = n[2]
		print(" $nbr,$v;")
	end
	println()
end
