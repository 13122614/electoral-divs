#!/usr/bin/perl -w
use strict;

## Usage: perl EDs-query.pl 'ED name'

use Switch 'Perl6';
use Geo::ShapeFile::Point comp_includes_z => 0, comp_includes_m => 0;
use Geo::ShapeFile;

sub findED($);
sub findCounty($);

my $shpFile = "Census2011_Electoral_Divisions_generalised20m.shp";
my $shpObj = new Geo::ShapeFile($shpFile);

my $inputED = shift() or die "Usage: perl EDs-query.pl 'ED name'\n";
my $exit = 0;
my @matches;
my $id, my $county, my $choice;

@matches = findED($inputED);
if($#matches == -1)
{
  print "No match for $inputED was found.\n";
  exit;
}
elsif($#matches > 0)
{
  print "Multiple instances found.\nPlease enter a county from the list below:\n";
  foreach my $i (@matches)
  {
    print "${$shpObj->get_dbf_record($i)}{'COUNTYNAME'}\n";
  }
  $county = <STDIN>;
  chomp $county;
  $id = findCounty($county);
  if(!$id)
  {
    print "No match for \'$county\' was found.\n";
    exit;
  }
}
else{
  $id = $matches[0];
}
while(!$exit)
{
  print "Please select from the options below:\n\n";
  print "1. Name of County and ID\n";
  print "2. Nomenclature of Territorial Units for Statistics (NUTS)\n";
  print "3. Central Statistics Office Electoral Division No. (CSOED)\n";
  print "4. Ordinance Survey Ireland Electoral Division No. (OSIED)\n";
  print "5. Population\n";
  print "6. Area\n";
  print "7. Housing Stock\n";
  print "0. Exit program\n";
  
  $choice = <STDIN>;
  chomp $choice;
  if($choice =~ /[0-7]{1}/)
  {
    if($choice != 0)
    {
      print "\n${$shpObj->get_dbf_record($id)}{'EDNAME'}\n";
    }
    given($choice)
    {
      when 0 { $exit = 1; }
      when 1 {
	print "${$shpObj->get_dbf_record($id)}{'COUNTYNAME'},\tID: ${$shpObj->get_dbf_record($id)}{'COUNTY'}";
      }
      when 2 {
	print "${$shpObj->get_dbf_record($id)}{'NUTS3NAME'},\t${$shpObj->get_dbf_record($id)}{'NUTS2NAME'},\t";
	print "${$shpObj->get_dbf_record($id)}{'NUTS1NAME'},\t${$shpObj->get_dbf_record($id)}{'NUTS3'}";
      }
      when 3 {
	print "CSOED No.: ${$shpObj->get_dbf_record($id)}{'CSOED'}";
      }
      when 4 {
	print "OSIED No.: ${$shpObj->get_dbf_record($id)}{'OSIED'}";
      }
      when 5 {
	print "Male: ", (int ${$shpObj->get_dbf_record($id)}{'Male2011'}), ",\tFemale: ", (int ${$shpObj->get_dbf_record($id)}{'Female2011'}), "\n";
	print "Total: ", (int ${$shpObj->get_dbf_record($id)}{'Total2011'});
      }
      when 6 {
	print "Area: ${$shpObj->get_dbf_record($id)}{'TOTAL_AREA'}\n";
	print "Of which is land: ${$shpObj->get_dbf_record($id)}{'LAND_AREA'}";
      }
      when 7 {
	print "Unoccupied dwellings: ", (int ${$shpObj->get_dbf_record($id)}{'Unocc2011'}), "\tOccupied dwellings: ", (int ${$shpObj->get_dbf_record($id)}{'PPOcc2011'}), "\n";
	print "Total: ", (int ${$shpObj->get_dbf_record($id)}{'HS2011'});
      }
    }
    print "\n\n";
  }
  else
  {
    print "Incorrect input. Enter an integer in the range 0-7:\n\n";
  }
}

sub findED($)
{
  my $n = shift or die $!;
  my @ids = ();
  my $totalEDS = 3409;
  my $id = 1;
  while($id <= $totalEDS)
  {
    if((lc $n) eq (lc ${$shpObj->get_dbf_record($id)}{'EDNAME'}))
    {
      push(@ids, $id);
    }
    $id++;
  }
  @ids;
}
sub findCounty($)
{
  my $c = shift or die $!;
  my $id;
  foreach (@matches)
  {
    if(${$shpObj->get_dbf_record($_)}{'COUNTYNAME'} =~ /$c/i)
    {
      $id = $_;
      last;
    }
  }
  $id;
}