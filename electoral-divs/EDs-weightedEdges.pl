#!/usr/bin/perl -w
use strict;

# Usage: perl EDS-points.pl |perl EDs-strictly_adj.pl | perl EDs-weightEdges.pl > weighted_edges

use Geo::ShapeFile::Point comp_includes_z => 0, comp_includes_m => 0;
use Geo::ShapeFile;

my $sfile = shift() || "Census2011_Electoral_Divisions_generalised20m.shp";
my $obj = new Geo::ShapeFile($sfile);

## key value pair(COUNTY(int val from .shp), true county)
## for now treating counties strictly as 26, irrespective of regions
## of high populations (e.g cities, Fingal, South Dublin, Dún Laoghaire-Rathdown)
my %sC2tC = (
  1 => "Carlow", 2 => "Dublin", 3 => "Dublin",
  4 => "Dublin", 5 => "Dublin", 6 => "Kildare",
  7 => "Kilkenny", 8 => "Laois", 9 => "Longford",
  10 => "Louth", 11 => "Meath", 12 => "Offaly",
  13 => "Westmeath", 14 => "Wexford", 15 => "Wicklow",
  16 => "Clare", 17 => "Cork", 18 => "Cork",
  19 => "Kerry", 20 => "Limerick", 21 => "Limerick",
  22 => "Tipperary", 23 => "Tipperary", 24 => "Waterford",
  25 => "Waterford", 26 => "Galway", 27 => "Galway",
  28 => "Leitrim", 29 => "Mayo", 30 => "Roscommon",
  31 => "Sligo", 32 => "Cavan", 33 => "Donegal",
  34 => "Monaghan"
);

## key value pair(County, Province)
## 1 = Leinster, 2 = Munster, 3 = Connacht, 4 = Ulster
my %cty2prov = (
  "Carlow" => 1, "Dublin" => 1, "Kildare" => 1,
  "Kilkenny" => 1, "Laois" => 1, "Longford" => 1,
  "Louth" => 1, "Meath" => 1, "Offaly" => 1,
  "Westmeath" => 1, "Wexford" => 1, "Wicklow" => 1,
  "Clare" => 2, "Cork" => 2, "Kerry" => 2,
  "Limerick" => 2, "Tipperary" => 2, "Waterford" => 2,
  "Galway" => 3, "Leitrim" => 3, "Mayo" => 3,
  "Roscommon" => 3, "Sligo" => 3, "Cavan" => 4,
  "Donegal" => 4, "Monaghan" => 4
);

print "## ED adjacencies with weighted edges ##\n";
print "## Edge values determined as follows:\n";
print "##\tWithin the same county: 2\n";
print "##\tWithin same province only: 1\n";
print "##\tIn different provinces: 0\n";
print "\n##################################\n\n";

my @split;
my $name = "";
my $county = 0;
while(<STDIN>){
  @split = split(/[\t\s]/, $_);
  $name = ${$obj->get_dbf_record($split[0])}{'EDNAME'};
  $county = int(${$obj->get_dbf_record($split[0])}{'COUNTY'});
  print "$split[0]. $name:\n\n";
  my $count = 1;
  while ($count <= $#split){
    my $edge = 0;
    my $n = ${$obj->get_dbf_record($split[$count])}{'EDNAME'};
    my $c = int(${$obj->get_dbf_record($split[$count])}{'COUNTY'});
    if(($county == $c) || ($sC2tC{$county} eq $sC2tC{$c})){
      $edge = 2;
    }elsif(($cty2prov{$sC2tC{$county}}) == ($cty2prov{$sC2tC{$c}})){
      $edge = 1;
    }
    print "$name\t-$edge-\t$split[$count]. $n\n";
    $count++;
  }
  print "\n##################################\n\n";
}













