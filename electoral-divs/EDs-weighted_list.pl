#!/usr/bin/perl -w
use strict;

# Usage: perl EDs-points.pl | perl EDs-strictly_adj.pl | perl EDs-weighted_list.pl > weighted_list

use Geo::ShapeFile::Point comp_includes_z => 0, comp_includes_m => 0;
use Geo::ShapeFile;

my $sfile = shift() || "Census2011_Electoral_Divisions_generalised20m.shp";
my $obj = new Geo::ShapeFile($sfile);

## key value pair(COUNTY(int val from .shp), true county)
## including all 'counties' defined in Census2011_Electoral_Divisions_generalised20m.shp
## this may help with groupings
my %sC2tC = (
  1 => "Carlow", 2 => "Dublin City", 3 => "South Dublin",
  4 => "Fingal", 5 => "Dún Laoghaire-Rathdown", 6 => "Kildare",
  7 => "Kilkenny", 8 => "Laois", 9 => "Longford",
  10 => "Louth", 11 => "Meath", 12 => "Offaly",
  13 => "Westmeath", 14 => "Wexford", 15 => "Wicklow",
  16 => "Clare", 17 => "Cork City", 18 => "Cork County",
  19 => "Kerry", 20 => "Limerick City", 21 => "Limerick County",
  22 => "North Tipperary", 23 => "South Tipperary", 24 => "Waterford City",
  25 => "Waterford County", 26 => "Galway City", 27 => "Galway County",
  28 => "Leitrim", 29 => "Mayo", 30 => "Roscommon",
  31 => "Sligo", 32 => "Cavan", 33 => "Donegal",
  34 => "Monaghan"
);

## key value pair(County, Province)
## 1 = Leinster, 2 = Munster, 3 = Connacht, 4 = Ulster
my %cty2prov = (
  "Carlow" => 1, "Dublin City" => 1, "South Dublin" => 1,
  "Fingal" => 1, "Dún Laoghaire-Rathdown" => 1, "Kildare" => 1,
  "Kilkenny" => 1, "Laois" => 1, "Longford" => 1,
  "Louth" => 1, "Meath" => 1, "Offaly" => 1,
  "Westmeath" => 1, "Wexford" => 1, "Wicklow" => 1,
  "Clare" => 2, "Cork City" => 2, "Cork County" => 2,
  "Kerry" => 2, "Limerick City" => 2, "Limerick County" => 2,
  "North Tipperary" => 2, "South Tipperary" => 2, "Waterford City" => 2,
  "Waterford County" => 2, "Galway City" => 3, "Galway County" => 3,
  "Leitrim" => 3, "Mayo" => 3, "Roscommon" => 3,
  "Sligo" => 3, "Cavan" => 4, "Donegal" => 4,
  "Monaghan" => 4
);

#print "## ED adjacencies with weighted edges ##\n";
#print "## Edge values determined as follows:\n";
#print "##\tWithin the same county: 2\n";
#print "##\tWithin same province only: 1\n";
#print "##\tIn different provinces: 0\n";
#print "\n##################################\n\n";

my @split;
my $name = "";
my $county = 0;
while(<STDIN>){
  @split = split(/[\t\s]/, $_);
  #$name = ${$obj->get_dbf_record($split[0])}{'EDNAME'};
  $county = int(${$obj->get_dbf_record($split[0])}{'COUNTY'});
  #print "$split[0]. $name:\n\n";
  
  my $count = 1;
  while ($count <= $#split){
    my $edge = 0;
    #my $n = ${$obj->get_dbf_record($split[$count])}{'EDNAME'};
    my $c = int(${$obj->get_dbf_record($split[$count])}{'COUNTY'});
    if(($county == $c) || ($sC2tC{$county} eq $sC2tC{$c})){
      $edge = 2;
    }elsif(($cty2prov{$sC2tC{$county}}) == ($cty2prov{$sC2tC{$c}})){
      $edge = 1;
    }
    #print "$name\t-$edge-\t$split[$count]. $n\n";
    print "$split[0],$edge,$split[$count]\n";
    $count++;
  }
  #print "\n##################################\n\n";
}













