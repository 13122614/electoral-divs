#!/home/jason/perl/bin/perl -w
##############################

# snagged from:
#   https://metacpan.org/source/SLAFFAN/Geo-ShapeFile-2.60/eg/shpdump.pl
# Usage:
#   perl EDs-points.pl [shapefile]

use strict;
use Geo::ShapeFile::Point comp_includes_z => 0, comp_includes_m => 0;
use Geo::ShapeFile;

use Data::Dumper;

my $sfile = shift() || "Census2011_Electoral_Divisions_generalised20m.shp";
my $obj = new Geo::ShapeFile($sfile);
 
for (1 .. $obj->shapes) {
    my ($shpId) = $_;

    my $shape = $obj->get_shp_record($_);

    foreach my $p (1 .. $shape->num_parts) {
        my @part = $shape->get_part($p);
        my $labeled = 0;
	pop @part;		# last one repeats first one: closes polygon
        for(@part) {
	    my ($x, $y) = ($_->X, $_->Y);
	    my $key = sprintf "%.5f,%.5f", $x, $y;
	    print $key, " $shpId\n";
        }
    }
}
