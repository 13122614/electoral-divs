#!/usr/bin/perl -w

use strict;
use Geo::ShapeFile::Point comp_includes_z => 0, comp_includes_m => 0;
use Geo::ShapeFile;

use Data::Dumper;

my $sfile = shift() || "Census2011_Electoral_Divisions_generalised20m.shp";
my $obj = new Geo::ShapeFile($sfile);

print "#\n# adjacancy graph in adjacancy list format\n";
print "# Three sections:\n";
print "#  1) adjacancy lists;\n";
print "#  2) IDs <--> Names\n";
print "#  3) Points <--> EDs\n";

my (%pt2ed) = ();
while (<STDIN>)
{
    my ($key, $shpId) = split /\s+/;
    push @{$pt2ed{$key}}, $shpId;
}
# now do an inefficient cull of the same EDs appearing more than once
# snagged from http://perlmaven.com/unique-values-in-an-array-in-perl
foreach my $key (keys %pt2ed)
{
    @{$pt2ed{$key}} = do { my %seen; grep { !$seen{$_}++ } @{$pt2ed{$key}} };
    @{$pt2ed{$key}} < 2 && next;
}

my $edgeCt = 0;
my (%adjs) = ();
foreach my $key (sort keys %pt2ed)
{
    foreach my $ed1 (@{$pt2ed{$key}})
    {
	foreach my $ed2 (@{$pt2ed{$key}})
	{
	    $ed1==$ed2 && next;
	    $adjs{$ed1}{$ed2} = $adjs{$ed2}{$ed1} = 1;
	    $edgeCt++;
	}
    }
}

#
# now print out adjs in DIMACS format
my $nodeCt = scalar keys %adjs;
print "# 1) adjacancy lists\n";
print "$nodeCt $edgeCt\n";

my $top;
my $topCt = 0;
foreach my $u (sort {$a <=> $b} keys %adjs)
{
    my $adjCt = scalar keys %{$adjs{$u}};
    print "$u\t", join(' ', sort {$a <=> $b} keys %{$adjs{$u}}), "\n";
    $edgeCt += $adjCt;

    $topCt < $adjCt || next;

    $topCt = $adjCt;
    $top = $u;
}

print "# 2) IDs <--> Names\n";
print "# Most surrounded: $top=${$obj->get_dbf_record($top)}{'EDNAME'}\n";
print "#  $top: ", join(' ', sort {$a <=> $b} keys %{$adjs{$top}}), "\n";
foreach my $sid (1..$obj->shapes)
{
    print "# $sid\t${$obj->get_dbf_record($sid)}{'EDNAME'}\n";
    defined $adjs{$sid} ||
	print STDERR " --- no adj list for $sid=${$obj->get_dbf_record($sid)}{'EDNAME'}\n";
}
print "# 3) Points <--> EDs\n";
foreach my $key (keys %pt2ed)
{
    print "# $key #", scalar @{$pt2ed{$key}}, " ", join(' ', @{$pt2ed{$key}}), "\n";
}
