#to use:
# python   in command line opens the python terminal, if installed
# >>>import myTree2 
# check location of CSO files relative to the myTree2.py file

# >>>myTree2.mainTest()  for testing
# or
# >>>Tree = myTree2.populate()  to fill a quadtree with the lines


import shapefile


# node has attributes type, parent, rect, children, LINES and BUCKETSIZE
# LINES has 2 coordinates (x1, y1, x2 and y2) and either 1 or 2 shapes 
# rect has 2 coordinates (top left, bottom right)


class node():

 ROOT = 0   # old code, not really used except maybe for testing
 BRANCH = 1
 LEAF = 2

 BUCKETSIZE = 500  # arbitrary value. Can be anything. Smaller bucket = slower but more accurate. Also, smaller buckets are at risk of causing a Maximum Recursion Depth Exceeded error! (top tip: the number 3 is probably too small)

 LINES = []  # format for lines goes x1, y1, x2, y2, shape(s) if any - later this is sorted so x1, y1 are the smallest values





 def __init__(self, parent, rect, children):

  self.parent = parent
  self.rect = rect
  if(children == None):
   self.children = [0,0,0,0]
  else:
   self.children = children
  

  x1, y1, x2, y2 = rect    

  if self.parent == None:
    self.type = node.ROOT
  elif self.children == [0,0,0,0]:
    self.type = node.LEAF
  else:
    self.type = node.BRANCH




 # exists IN THE LIST OF LINES, not exists within a bounding box
 def exists(self, x1, y1, x2, y2):
  for i in self.LINES:
   if (i[0] == x1 and i[1] == y1 and i[2] == x2 and i[3] == y2):
    return True
  return False




# is the top left point of a line within the boundaries of a node's rect?
 def contains(self, x1, y1):
  if (x1 >= self.rect[0] and x1 <= self.rect[2] and y1 >= self.rect[1] and y1 <= self.rect[3]):# reminder: [x1,y1,x2,y2] at xth position... not working properly :(
   return True
  else:
   return False











 def insert(self, x1, y1, x2, y2, shape):

  # find smallest point on line, assign to mainX/mainY
  # better for sorting purposes
  # bugger to test though
  if (x1 < x2):
   mainX = x1 
   mainY = y1 
   altX = x2
   altY = y2
  elif (x2 < x1):
   mainX = x2
   mainY = y2
   altX = x1
   altY = y1
  else: 	# if X values are equal, sort by Y
   if (y1 < y2):
    mainX = x1
    mainY = y1
    altX = x2
    altY = y2
   else:        # Xs AND Ys should never be identical; Lines, not points
    mainX = x2
    mainY = y2
    altX = x1
    altY = y1
  
  #shape refers to a shape's identifier; Python allows this to be anything at all. Since a line divides a space into two sides, each line will ultimately end up with maximum 2 shapes associated with it (1 is for coastal borders and the like).
  shapes = []
  shapes.append(shape)

  newline = [mainX, mainY, altX, altY, shapes]
  if (self.children == [0,0,0,0]):
  # no kids
   # a lot of opportunity for errors here though. Complicated


   # if line exists in node already, you want to append the new shape to it
   if (self.exists(mainX, mainY, altX, altY)):

    for i in self.LINES:
     if (i[0] == mainX and i[1] == mainY and i[2] == altX and i[3] == altY): # i is a line, i[0] is the "x1" of that line
      i[4].append(shape)   #add new shape to previously set "shapes" in older line


  # line not already in node
  # check to ensure bucket limit is maintained
  # if not, subdivide (recurse!)
   elif ((len(self.LINES) + 1) <= self.BUCKETSIZE ):  # +1 for filling the bucket but not overflowing it

    self.LINES.append(newline)         # recursion base case

   else:			       # over bucket limit

    self.LINES.append(newline)
    self.subdivide()                   # recursive add
  
  else:  # reminder: if node DOES have kids
         # must locate correct subnode and add to that instead
   
   for n in range(4):
    if (self.children[n].contains(mainX, mainY)):
     self.children[n].insert(x1, y1, x2, y2, shape)  
   # more recursion
     break

# note differences in use between insert and subdivide














 def subdivide(self):	# splits a node so it gets 4 children
  x1,y1,x2,y2 = self.rect
  hx = (x2 - x1)/2 	#  "half of side x"
  hy = (y2 - y1)/2

  # create boundaries for sub-nodes
  rects = []
  rects.append( (x1, y1, x1 + hx, y1 + hy) ) # top left
  rects.append( (x1, y1 + hy, x1 + hx, y2) ) # bottom left
  rects.append( (x1 + hx, y1, x2, y1 + hy) ) # top right
  rects.append( (x1 + hx, y1 + hy, x2, y2) ) # bottom right


	# added four rects
	# create child from rects
	# add each child to the parent	
	# reminder: each NODE has a list of LINES which store information


  self.children = []    # otherwise the children will be [0,0,0,0,child1, child2...] etc
  
  for n in range(4):    # for each of the four subrectangles
   child  = node(self, rects[n], [0,0,0,0])	# create a node which uses a rect and references the parent
   child.LINES = []  		 # surprisingly, the children keep the parent's lines by default, and must be reset on creation. 
						# It was fun figuring that out :|
   self.children.append(child)		# then add the child to the list of children stored in a node
	

  for n in range(len(self.LINES)):   # move through each line stored in the parent
   for i in range(4):	             # move line to child node based on which child's rect the line's X1,Y1 belongs in	     
    if self.children[i].contains(self.LINES[n][0], self.LINES[n][1]):   # smallest coordinate, previously sorted
     self.children[i].insert(self.LINES[n][0],self.LINES[n][1],self.LINES[n][2],self.LINES[n][3],self.LINES[n][4])   # even more recursion
     break	     
  
  self.LINES = []
  # and clear them from the parent!	

        


 






def getBBOX():
#  sf = shapefile.Reader("shapefiles/cso/electoral-divs/electoral_divisions")
  sf = shapefile.Reader("electoral-divs/electoral_divisions")
  shapes = sf.shapes()

  x1 = float("inf")    
  y1 = float("inf")
  x2 = -1 * float("inf")  # safer than 0 or a very small number; map may use a negative coordinate
  y2 = -1 * float("inf")

  for i in range(len(shapes)):   # better way of doing this? Brute force is slow and messy
   bbox = shapes[i].bbox

   if bbox[0] < x1:
     x1 = bbox[0]
   if bbox[1] > y2:
     y2 = bbox[1]
   if bbox[2] > x2:
     x2 = bbox[2]
   if bbox[3] < y1:
     y1 = bbox[3]
  return [x1,y1,x2,y2]





def populate():
 #warning: LARGE! Run at own risk! n^2 (not counting the runtime of recursive Inserts...) using a really big file
 #maybe watch a movie while you wait
 sf = shapefile.Reader("shapefiles/cso/electoral-divs/electoral_divisions")
 shapes = sf.shapes()
  
 rect = getBBOX()
 root = node(None, rect, None)
 x = 0
  
 for i in range(len(shapes)):  #iterate through shapes. 
				#We'll use i as a placeholder for shape ID
  pointTotal = len(shapes[i].points)
  for n in range(pointTotal):  #iterate through points (and therefore lines) on that shape
   root.insert(shapes[i].points[n][0], shapes[i].points[n][1], shapes[i].points[(n+1)%pointTotal][0], shapes[i].points[(n+1)%pointTotal][1], i)
   x = x+1
   number = str(x)
   print("Lines iterated through:                                " + number)  #this is here purely to see something work on-screen
   #adds lines to root node: Line is made up of a point on the shape, and the next point after it (n+1)
   #we mod the number by the list of points in order for the lines to wrap around and hit 0 (the first point) again at the end.

 return root







def mainTest():
 rect = getBBOX()
 root = node(None, rect, None)
 root.BUCKETSIZE = 3
 print("Children/Type test (0 is root)")
 print(root.children)
 print(root.type)
 print("   ")
 root.insert(18000,29000,18000,450000,3)
 print(root.LINES)
 root.insert(18000,29000,18000,450000,4)   # "additional shape" test. Really, this can never be greater than two different shapes per line.
 print(root.LINES)
 root.insert(20000,40000,18000,400000,8)
 print(root.LINES)
 root.insert(220000,333333,220000,450000,76)
 print(root.LINES)

 root.insert(30500,29050,70050,40050,0)
 print("lines after bucket overflow: (should be null)")

 print(root.LINES)
 print(root.children)
 print(root.children[0].LINES)  # should have two
 print(root.children[1].LINES)  # one
 print(root.children[2].LINES)  # zero
 print(root.children[3].LINES)  # one

 # post-overflow: adding to root with kids
 root.insert(230000, 190000, 320000, 100000, 50)
 print(root.children[0].LINES)  # still two
 print(root.children[1].LINES)  # one
 print(root.children[2].LINES)  # ONE
 print(root.children[3].LINES)  # one
 

 print(" ")
 print(" ")
 print(" ")
 print(" ")
 print(" ")
 print(" ")

 # testing for grandchildren
 root.insert(150003,29020,150010,30000,17) # insert into child 1
 root.insert(170000,230700,170500,230000,6)  # overflow
 print("       ")
 print("Root lines: ")
 print(root.LINES)
 print("Children lines: ")
 print(root.children[0].LINES)  # now zero!
 print(root.children[1].LINES)  # one
 print(root.children[2].LINES)  # one
 print(root.children[3].LINES)  # one
 print("Grandchildren?")
 print(root.children[0].children)  # should now have kids
 print(root.children[1].children)  # all others are empty
 print(root.children[2].children)
 print(root.children[3].children)
 print("Grandchildren lines")
 print(root.children[0].children[0].LINES)
 print(root.children[0].children[1].LINES)
 print(root.children[0].children[2].LINES)
 print(root.children[0].children[3].LINES)
